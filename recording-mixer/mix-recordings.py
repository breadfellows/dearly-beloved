import glob
import sox
from datetime import datetime
cbn = sox.Combiner()
from subprocess import call

current_datetime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
web_recorder_dl_folder = "/home/colm/"
all_recordings = glob.glob(web_recorder_dl_folder + "*.wav")
backing_track = "/home/colm/git/dearly-beloved/recording-mixer/backing-audio/Bass&Drums_48000.wav"
backing_track_drums_only = "/home/colm/git/dearly-beloved/recording-mixer/backing-audio/Drums_48000_2.wav"
backing_track_everything = "/home/colm/git/dearly-beloved/recording-mixer/backing-audio/Everything_48000_2.wav"
output_path1 = "/home/colm/git/dearly-beloved/recording-mixer/output/merged"
output_path2 = "/home/colm/git/dearly-beloved/recording-mixer/output/merged-full-tracks/"
merged_file = glob.glob(output_path1 + "*.wav")
list_to_concat = []

for file in all_recordings:
    print(file)
    voice_file = sox.file_info.info(file)
    print(voice_file)

print(backing_track)
print(sox.file_info.info(backing_track))

for index, file in enumerate(all_recordings):
    index = str(index)
    output_path = str(output_path1 + index + ".wav")
    print(output_path)
    cbn.build([file, backing_track], output_path, 'mix-power')

for merges in merged_file:
    print(merges)
    list_to_concat.append(merges)


list_to_concat.insert(0, backing_track_drums_only)
list_to_concat.insert(3, backing_track_everything)
print("about to concat: ")
print(list_to_concat)
output_path = str(output_path2 + current_datetime + ".wav")
latest_path = str(output_path2 + 'latest.wav')
print("Generating : " + output_path)
cbn.build(list_to_concat, output_path, 'concatenate')
cbn.build(list_to_concat, latest_path, 'concatenate')
# this will only work if connected to project network Dearly_Beloved
cmd = "scp " + latest_path + " pi@raspberrypi.local:/home/pi/"
print(cmd)
call(cmd.split(" "))
