import nltk
import random

text = open('/home/colm/git/dearly-beloved/lyrics-scraping/all_lyrics_t50.txt')

f=open('/home/colm/git/dearly-beloved/lyrics-scraping/all_lyrics_t50-cond.txt','r')
TEXT=f.read()
# tokens = nltk.word_tokenize(raw)

# NLTK shortcuts :)
bigrams = nltk.bigrams(TEXT)
cfd = nltk.ConditionalFreqDist(bigrams)

# pick a random word from the corpus to start with
word = random.choice(TEXT)
# generate 15 more words
for i in range(15):
    print(word)
    if word in cfd:
        word = random.choice(list(cfd[word].keys()))
    else:
        break
