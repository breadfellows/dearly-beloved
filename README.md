#  Dearly beloved,
## We are gathered here today <br> To get through this thing called LIFE.

This repository contains the code and graphics basis for the engagement and responsive space for the *ARTWORKS 2019* group show at VISUAL Carlow

https://www.visualcarlow.ie/exhibitions/info/a-collaborative-love-song

![](media/IMG_20190601_174306.jpg)

Within this project we publish the code used in the various media installations of the space, as well as some of the graphical elements developped and adapted for the space.

This space is being programmed and developped by VISUAL resident artist [Clare](clarebreen.net) [Breen](breadfellows.school) and media artist / designer [Colm O'Neill](colm.be)

Unless otherwise noted the code published here is licenced [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html). Orignal images or symbols remain under original producer copyright.


## Anatomy of the repository :

Subfolders in this project are used to separate different projects of the installation. These are detailed below, if needed, the subfolder may contain a `notes.md` file for more detail on its contents.

* lyrics-scraper/

Contains lyric list sources and the scraping script that reads a csv file to query WikiLyric to retrive actual lyric text. See lyrics-scraping/notes.md

* lyric-generator/

Uses the output of the lyrics-scraping process as a basis to build a corpus for the various NLTK (Natural Language Tool Kit) processes

* web-recorder/

Contains the audio recorder that runs in the recording booth. This is a website that makes use of `getUserMedia()` functions in modern browsers to allow microphone input. The main library in use is [web-audio-recorder-js](https://github.com/higuma/web-audio-recorder-js) and namely the use example from https://blog.addpipe.com/. The web-recorder is the interface to capture visitor's voices to be later mixed into The Collaborative Love Song.

* recording-mixer/

Contains the scripts that mix the captured audio onto the backing track, then generates The Song by placing these snippets back to back. Main dependency here is PySox. Original backing music by Richard Breen.

* graphics/  

Contains the fonts, graphics and any graphic design that supports the installation. Vinyl cuts etc. Fonts in use are [Victoria](http://www.peter-wiegel.de/Victoria.html) by Peter Wiegel (Licence forthcoming) and [HK Grotest](https://hanken.co/hk-grotesk/) by Alfredo Marco Pradil and Arlene Arabe.

* media/

Photos of the installation, a place to keep the other things

![](media/IMG_20190601_215544.jpg)

![](media/Screenshot_from_2019-05-31_18-47-27.png)

### Tech notes:

To access gnome-control-center from i3 (for audio input and output graphical control) run :

`env XDG_CURRENT_DESKTOP=GNOME gnome-control-center`

info from https://sakhnik.com/2018/08/02/i3-gnome.html

![](graphics/love-symbol.png)

### Installation images :

![](installation/VISClareBreen100.jpg)

![](installation/VISClareBreen101.jpg)

![](installation/VISClareBreen102.jpg)

![](installation/VISClareBreen103.jpg)

![](installation/VISClareBreen104.jpg)

![](installation/VISClareBreen105.jpg)

![](installation/VISClareBreen106.jpg)

![](installation/VISClareBreen107.jpg)

![](installation/VISClareBreen108.jpg)

![](installation/VISClareBreen109.jpg)

![](installation/VISClareBreen110.jpg)

![](installation/VISClareBreen111.jpg)

![](installation/VISClareBreen112.jpg)

![](installation/VISClareBreen113.jpg)
