# A Collective Love Song

<!-- General intro -->
## Dearly Beloved,
You are invited to write, perform and record a collaboratively written song.

 <!-- Lyric writing station -->
 ## Lyric Writing
We collected lyrics from over 200 love songs*.
We developed a word analysis software to gather the lyrics of all of these songs and to find the most common words.
These words are available as a base for you to play around with and then write your own lyrics.
Take a selection of these words and collage them to form your song line.
If there is a word you'd like to include that we haven't provided, just flip a card over and write your own.
Glue down your lyrics and take them to the recording booth.

Note: You can record for 8 seconds, this is not very long so try to make your line short!

*These songs include the top charting international pop songs from every year since 1969, billboard top 100 charts, Rolling Stone's top love songs,
69 love songs, the top most played wedding songs of all time (according to the internet) and the favourite love songs of our friends and the staff at VISUAL.   

 ## Recording
 Inside the booth you can record your lyrics. When you're happy with your recording, you can add it to The Collective Love Song.

 ## Listening
 Inside these headphones plays The Collective Love song. The Collective Love Song is made up of individual lines from individual people.

 ## Performing
 You can use the stage to practice or to perform, it's up to you. 
