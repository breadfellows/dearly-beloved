# Notes for the lyric collecting (called scraping) process

Sources for the listing are:
* https://www.billboard.com/articles/list/6792625/top-50-love-songs-of-all-time
* http://tsort.info/music/years0.htm
* https://www.billboard.com/articles/columns/rock/7487656/top-rock-songs-about-love-greatest-ever-classics
* https://www.theknot.com/content/best-love-songs

All song title, artist & source collected in file dearly_beloved_lyric_sources.ods

lyric scraping script is `lyric-scraper.py` and depends on [PyLyrics](https://github.com/geekpradd/PyLyrics)
