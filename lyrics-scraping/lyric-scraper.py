import csv
from PyLyrics import *
import urllib.parse
from datetime import datetime
current_datetime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
f = open("all_lyrics" + current_datetime + ".txt","w+")
r = open("rejected_tracks" + current_datetime + ".txt", "w+")
with open('dearly_beloved_lyric_sources.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        print("\n")
        artist = urllib.parse.quote(row[1],safe='')
        track = urllib.parse.quote(row[2],safe='')
        try:
            print(artist + ', ' + track)
            print("-----")
            lyrics = PyLyrics.getLyrics(artist,track)
            print(lyrics)
            f.write(lyrics)
        except ValueError:
            print("!!!")
            print(artist + ', ' + track + "NOT RETRIEVED")
            print("!!!")
            r.write("\n" + "\n" + row[1] + ', ' + row[2] + " !!! NOT RETRIEVED !!! " + "\n" + "\n" )
