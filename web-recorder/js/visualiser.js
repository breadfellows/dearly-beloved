(function(p) {
	var b = function() {};
	b.__name__ = !0;
	b.main = function() {
		window.onload = function() {
			b.div = window.document.getElementById("audio-div");
			b.canvas = window.document.getElementById("audio-canvas");
			null == b.canvas && (b.canvas = window.document.createElement("canvas"), b.canvas.id = "audio-canvas", b.div.appendChild(b.canvas));
			b.canvas.width = b.div.clientWidth;
			b.canvas.height = b.div.clientHeight;
			b.userMediaInfoDisplay = window.document.getElementById("audio-info");
			null == b.userMediaInfoDisplay && (b.userMediaInfoDisplay =
				window.document.createElement("p"), b.userMediaInfoDisplay.id = "audio-info", b.userMediaInfoDisplay.style.font = '13pt "hk-med", sans-serif', b.userMediaInfoDisplay.style.textAlign = "center", b.userMediaInfoDisplay.style.padding = "8px", b.userMediaInfoDisplay.style.position = "absolute", b.userMediaInfoDisplay.style.top = "45%", b.userMediaInfoDisplay.style.width = "100%", b.userMediaInfoDisplay.style.color = "#111111", b.div.appendChild(b.userMediaInfoDisplay));
			b.userMediaInfoDisplay.innerHTML = "Waiting for microphone...";
			b.actx = new AudioContext;
			b.analyser = b.actx.createAnalyser();
			navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
			navigator.getUserMedia({
				audio: !0
			}, b.onGetUserMedia, b.onGetUserMediaFailed);
			b.ctx = b.canvas.getContext("2d")
		}
	};
	b.onGetUserMediaFailed = function(a) {
		window.console.log("Getting user media failed: " + m.string(a));
		b.userMediaInfoDisplay.innerHTML = "NotFoundError" ==
			a.name ? "<strong>We can't find your microphone</strong> - that likely means that it's either not connected, broken, or you don't have the proper webcam drivers (see below)" : "Microphone not authorized. Please check your media permissions settings."
	};
	b.onGetUserMedia = function(a) {
		b.userMediaInfoDisplay.style.display = "none";
		b.source = b.actx.createMediaStreamSource(a);
		b.source.connect(b.analyser);
		b.visualize(a)
	};
	b.visualize = function(a) {
		b.ctx.clearRect(0, 0, b.canvas.width, b.canvas.height);
		var d = .5 *
			b.canvas.height,
			c, v = b.canvas.width / b.analyser.frequencyBinCount;
		c = .5 * b.canvas.height;
		var e = new w(b.analyser.frequencyBinCount),
			f = null,
			f = function() {
				window.requestAnimationFrame(f);
				b.analyser.getFloatTimeDomainData(e);
				b.ctx.fillStyle = "white";
				b.ctx.fillRect(0, 0, b.canvas.width, b.canvas.height);
				b.ctx.lineWidth = 1.5;
				b.ctx.strokeStyle = "#019961";
				b.ctx.beginPath();
				for (var a = 0, k = e.length; a < k;) {
					var g = a++;
					b.ctx.lineTo(g * v, e[g] * c + d)
				}
				b.ctx.stroke()
			};
		f()
	};
	Math.__name__ = !0;
	var m = function() {};
	m.__name__ = !0;
	m.string = function(a) {
		return f.__string_rec(a,
			"")
	};
	var n = function() {};
	n.__name__ = !0;
	n.i32ToFloat = function(a) {
		var d = a >>> 23 & 255,
			c = a & 8388607;
		return 0 == c && 0 == d ? 0 : (1 - (a >>> 31 << 1)) * (1 + Math.pow(2, -23) * c) * Math.pow(2, d - 127)
	};
	n.floatToI32 = function(a) {
		if (0 == a) return 0;
		var d = 0 > a ? -a : a,
			c = Math.floor(Math.log(d) / .6931471805599453); - 127 > c ? c = -127 : 128 < c && (c = 128);
		d = Math.round(8388608 * (d / Math.pow(2, c) - 1));
		8388608 == d && 128 > c && (d = 0, ++c);
		return (0 > a ? -2147483648 : 0) | c + 127 << 23 | d
	};
	var k = function(a) {
		Error.call(this);
		this.val = a;
		this.message = String(a);
		Error.captureStackTrace &&
			Error.captureStackTrace(this, k)
	};
	k.__name__ = !0;
	k.wrap = function(a) {
		return a instanceof Error ? a : new k(a)
	};
	k.__super__ = Error;
	k.prototype = function(a, d) {
		function c() {}
		c.prototype = a;
		var b = new c,
			e;
		for (e in d) b[e] = d[e];
		d.toString !== Object.prototype.toString && (b.toString = d.toString);
		return b
	}(Error.prototype, {
		__class__: k
	});
	var f = function() {};
	f.__name__ = !0;
	f.getClass = function(a) {
		if (a instanceof Array && null == a.__enum__) return Array;
		var d = a.__class__;
		if (null != d) return d;
		a = f.__nativeClassName(a);
		return null !=
			a ? f.__resolveNativeClass(a) : null
	};
	f.__string_rec = function(a, d) {
		if (null == a) return "null";
		if (5 <= d.length) return "<...>";
		var c = typeof a;
		"function" == c && (a.__name__ || a.__ename__) && (c = "object");
		switch (c) {
			case "function":
				return "<function>";
			case "object":
				if (a instanceof Array) {
					if (a.__enum__) {
						if (2 == a.length) return a[0];
						c = a[0] + "(";
						d += "\t";
						for (var b = 2, e = a.length; b < e;) var g = b++,
							c = 2 != g ? c + ("," + f.__string_rec(a[g], d)) : c + f.__string_rec(a[g], d);
						return c + ")"
					}
					c = a.length;
					b = "[";
					d += "\t";
					for (e = 0; e < c;) g = e++, b += (0 < g ? "," : "") +
						f.__string_rec(a[g], d);
					return b + "]"
				}
				try {
					b = a.toString
				} catch (k) {
					return "???"
				}
				if (null != b && b != Object.toString && "function" == typeof b && (c = a.toString(), "[object Object]" != c)) return c;
				c = null;
				b = "{\n";
				d += "\t";
				e = null != a.hasOwnProperty;
				for (c in a) e && !a.hasOwnProperty(c) || "prototype" == c || "__class__" == c || "__super__" == c || "__interfaces__" == c || "__properties__" == c || (2 != b.length && (b += ", \n"), b += d + c + " : " + f.__string_rec(a[c], d));
				d = d.substring(1);
				return b + ("\n" + d + "}");
			case "string":
				return a;
			default:
				return String(a)
		}
	};
	f.__interfLoop = function(a, d) {
		if (null == a) return !1;
		if (a == d) return !0;
		var c = a.__interfaces__;
		if (null != c)
			for (var b = 0, e = c.length; b < e;) {
				var g = b++,
					g = c[g];
				if (g == d || f.__interfLoop(g, d)) return !0
			}
		return f.__interfLoop(a.__super__, d)
	};
	f.__instanceof = function(a, d) {
		if (null == d) return !1;
		switch (d) {
			case Array:
				return a instanceof Array ? null == a.__enum__ : !1;
			case r:
				return "boolean" == typeof a;
			case x:
				return !0;
			case t:
				return "number" == typeof a;
			case y:
				return "number" == typeof a ? (a | 0) === a : !1;
			case String:
				return "string" == typeof a;
			default:
				if (null != a)
					if ("function" == typeof d) {
						if (a instanceof d || f.__interfLoop(f.getClass(a), d)) return !0
					} else {
						if ("object" == typeof d && f.__isNativeObj(d) && a instanceof d) return !0
					}
				else return !1;
				return d == z && null != a.__name__ || d == A && null != a.__ename__ ? !0 : a.__enum__ == d
		}
	};
	f.__nativeClassName = function(a) {
		a = f.__toStr.call(a).slice(8, -1);
		return "Object" == a || "Function" == a || "Math" == a || "JSON" == a ? null : a
	};
	f.__isNativeObj = function(a) {
		return null != f.__nativeClassName(a)
	};
	f.__resolveNativeClass = function(a) {
		return p[a]
	};
	var g = function(a) {
		if (a instanceof Array && null == a.__enum__) this.a = a, this.byteLength = a.length;
		else {
			this.a = [];
			for (var d = 0; d < a;) {
				var c = d++;
				this.a[c] = 0
			}
			this.byteLength = a
		}
	};
	g.__name__ = !0;
	g.sliceImpl = function(a, d) {
		var c = new u(this, a, null == d ? null : d - a),
			b = new q(c.byteLength);
		(new u(b)).set(c);
		return b
	};
	g.prototype = {
		slice: function(a, d) {
			return new g(this.a.slice(a, d))
		},
		__class__: g
	};
	var l = function() {};
	l.__name__ = !0;
	l._new = function(a, d, c) {
		var b;
		if ("number" == typeof a) {
			b = [];
			for (d = 0; d < a;) {
				var e = d++;
				b[e] = 0
			}
			b.byteLength =
				b.length << 2;
			b.byteOffset = 0;
			a = [];
			d = 0;
			for (e = b.length << 2; d < e;) d++, a.push(0);
			b.buffer = new g(a)
		} else if (f.__instanceof(a, g)) {
			null == d && (d = 0);
			null == c && (c = a.byteLength - d >> 2);
			b = [];
			for (e = 0; e < c;) {
				e++;
				var h = a.a[d++] | a.a[d++] << 8 | a.a[d++] << 16 | a.a[d++] << 24;
				b.push(n.i32ToFloat(h))
			}
			b.byteLength = b.length << 2;
			b.byteOffset = d;
			b.buffer = a
		} else if (a instanceof Array && null == a.__enum__) {
			b = a.slice();
			a = [];
			for (d = 0; d < b.length;) e = b[d], ++d, e = n.floatToI32(e), a.push(e & 255), a.push(e >> 8 & 255), a.push(e >> 16 & 255), a.push(e >>> 24);
			b.byteLength =
				b.length << 2;
			b.byteOffset = 0;
			b.buffer = new g(a)
		} else throw new k("TODO " + m.string(a));
		b.subarray = l._subarray;
		b.set = l._set;
		return b
	};
	l._set = function(a, b) {
		if (f.__instanceof(a.buffer, g)) {
			if (a.byteLength + b > this.byteLength) throw new k("set() outside of range");
			for (var c = 0, h = a.byteLength; c < h;) {
				var e = c++;
				this[e + b] = a[e]
			}
		} else if (a instanceof Array && null == a.__enum__) {
			if (a.length + b > this.byteLength) throw new k("set() outside of range");
			c = 0;
			for (h = a.length; c < h;) e = c++, this[e + b] = a[e]
		} else throw new k("TODO");
	};
	l._subarray =
		function(a, b) {
			var c = l._new(this.slice(a, b));
			c.byteOffset = 4 * a;
			return c
		};
	var h = function() {};
	h.__name__ = !0;
	h._new = function(a, b, c) {
		if ("number" == typeof a) {
			c = [];
			for (b = 0; b < a;) {
				var l = b++;
				c[l] = 0
			}
			c.byteLength = c.length;
			c.byteOffset = 0;
			c.buffer = new g(c)
		} else if (f.__instanceof(a, g)) null == b && (b = 0), null == c && (c = a.byteLength - b), c = 0 == b ? a.a : a.a.slice(b, b + c), c.byteLength = c.length, c.byteOffset = b, c.buffer = a;
		else if (a instanceof Array && null == a.__enum__) c = a.slice(), c.byteLength = c.length, c.byteOffset = 0, c.buffer = new g(c);
		else throw new k("TODO " +
			m.string(a));
		c.subarray = h._subarray;
		c.set = h._set;
		return c
	};
	h._set = function(a, b) {
		if (f.__instanceof(a.buffer, g)) {
			if (a.byteLength + b > this.byteLength) throw new k("set() outside of range");
			for (var c = 0, h = a.byteLength; c < h;) {
				var e = c++;
				this[e + b] = a[e]
			}
		} else if (a instanceof Array && null == a.__enum__) {
			if (a.length + b > this.byteLength) throw new k("set() outside of range");
			c = 0;
			for (h = a.length; c < h;) e = c++, this[e + b] = a[e]
		} else throw new k("TODO");
	};
	h._subarray = function(a, b) {
		var c = h._new(this.slice(a, b));
		c.byteOffset = a;
		return c
	};
	String.prototype.__class__ = String;
	String.__name__ = !0;
	Array.__name__ = !0;
	var y = {
			__name__: ["Int"]
		},
		x = {
			__name__: ["Dynamic"]
		},
		t = Number;
	t.__name__ = ["Float"];
	var r = Boolean;
	r.__ename__ = ["Bool"];
	var z = {
			__name__: ["Class"]
		},
		A = {},
		q = p.ArrayBuffer || g;
	null == q.prototype.slice && (q.prototype.slice = g.sliceImpl);
	var w = p.Float32Array || l._new,
		u = p.Uint8Array || h._new;
	f.__toStr = {}.toString;
	l.BYTES_PER_ELEMENT = 4;
	h.BYTES_PER_ELEMENT = 1;
	b.main()
})("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" !=
	typeof self ? self : this);
