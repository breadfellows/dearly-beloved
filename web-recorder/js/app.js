//webkitURL is deprecated but nevertheless
URL = window.URL || window.webkitURL;

var gumStream; 						//stream from getUserMedia()
var recorder; 						//WebAudioRecorder object
var input; 							//MediaStreamAudioSourceNode  we'll be recording
var encodingType; 					//holds selected encoding for resulting audio (file)
var encodeAfterRecord = true;       // when to encode
var audioStream;
var maxRecTime;

// shim for AudioContext when it's not avb.
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext; //new audio context to help us record

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");
var recAnim = document.getElementsByClassName("spinner-rec");

var recordingAnimString = "<div class='spinner-grow spinner-rec' style='width: 1.5rem; height: 1.5rem;' role='status'><span class='sr-only'>Loading...</span></div>&nbsp;Recording ...&nbsp;<div class='spinner-grow spinner-rec' style='width: 1.5rem; height: 1.5rem;' role='status'><span class='sr-only'>Loading...</span></div>"

//add events to those 2 buttons
recordButton.addEventListener("click", startRecording);
// stopButton.addEventListener("click", stopRecording);

document.addEventListener("DOMContentLoaded", function(event) {
	var constraints = {
    audio: true,
    video: false
	}
	navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
    audioStream = stream;
		console.log(audioStream);
	}).catch(function(err) {
		recordButton.disabled = false;
		stopButton.disabled = true;
	});
	// updateAnalysers();
	//play();
});

function startRecording() {
	console.log("startRecording() called");
	audioContext = new AudioContext();
	input = audioContext.createMediaStreamSource(audioStream);

	recorder = new WebAudioRecorder(input, {
	  workerDir: "js/", // must end with slash
	  encoding: 'wav',
	  numChannels:2, //2 is the default, mp3 encoding supports only 2
	});

	recorder.onComplete = function(recorder, blob) {
		createDownloadLink(blob,recorder.encoding);
		encodingTypeSelect.disabled = false;
	}

	recorder.setOptions({
	  timeLimit:8,
	  encodeAfterRecord:encodeAfterRecord,
	     ogg: {quality: 0.8},
	     mp3: {bitRate: 160}
 	 });

	 var countIn = 4;
	 var countInElem = document.getElementById("countIn")
	 var countdownElem = document.getElementById("countdown")
	 $("#countIn").css({"display":"block"});
	 var countInTimer = setInterval(function(){
		 countInElem.innerHTML = "Recording starts in: " + countIn;
		 countIn -= 1;
		 if(countIn <= 0){
			 clearInterval(countInTimer);
			 countInElem.innerHTML = "Sing now !"
			 $('.spinner-rec').toggle();
			 recordButton.innerHTML = recordingAnimString;
			 var backingTrack = new Audio('Drums_44100.wav');
			 backingTrack.play();
			 recorder.startRecording();
			 recordButton.disabled = true;
			 var timeleft = 8;
			 var downloadTimer = setInterval(function(){
	 		 countdownElem.innerHTML = timeleft + " seconds remaining";
	 		 timeleft -= 1;
			 $("#countdown").css({"display":"block"});
	 		 if(timeleft <= 0){
				 clearInterval(downloadTimer);
				 countdownElem.innerHTML = "Finished";
				 $("#countdown").css({"display":"none"});
				 $('.spinner-rec').toggle();
				 stopRecording();
				 countInElem.innerHTML = "Recording done, listen below"
			 }
		 }, 1000);
	 }
 }, 1000);
}

function stopRecording() {
	console.log("stopRecording() called");
	$('.spinner-rec').hide();
	recordButton.innerHTML = "Done recording"
	recordButton.disabled = true;
	recorder.finishRecording();
	$("#recording-list").show();
	$("#countdown").css({"display":"none"});
	$("#countIn").css({"display":"none"});

}

function createDownloadLink(blob,encoding) {

	var url = URL.createObjectURL(blob);
	var au = document.createElement('audio');
	var li = document.createElement('li');
	var link = document.createElement('a');
	var submit = document.createElement('a');
	submit.classList.add('btn', 'btn-success', 'submit-recording-btn');
	submit.addEventListener('click', acceptRecordingEvent, false);
	var tryAgainBtn = document.createElement('a');
	tryAgainBtn.classList.add('btn', 'btn-secondary', 'refuse-recording-btn')
	tryAgainBtn.addEventListener('click', refusalEvent, false);

	//add controls to the <audio> element
	au.controls = true;
	au.src = url;

	//link the a element to the blob
	//link.href = url;
	link.download = new Date().toISOString() + '.'+encoding;
	link.innerHTML = link.download;

	//creating a submit buttons
	submit.href = url;
	submit.download = new Date().toISOString() + '.'+encoding;
	submit.innerHTML = "Yes, send this recording to be processed";

	tryAgainBtn.innerHTML = "No, I want to record again";
	tryAgainBtn.href = "#";

	//add the new audio and a elements to the li element
	li.appendChild(au);
	// li.appendChild(link);
	li.appendChild(submit);
	li.appendChild(tryAgainBtn)

	//add the li element to the ordered list
	recordingsList.appendChild(li);
}

function refusalEvent(){
	console.log("Recording refused, flushing and re-enabling interface");
	parentEl = $(this).parent();
	console.log(parentEl);
	parentEl.remove();
	$("#recording-list").hide();
	recordButton.innerHTML = "Record"
	recordButton.disabled = false;
	countdownElem.innerHTML = "";
}

function acceptRecordingEvent(){
	console.log("recording accepted by singer, downloading file");
	parentEl = $(this).parent();
	console.log(parentEl);
	parentEl.remove();
	$("#recording-list").hide();
	recordButton.disabled = false;
	recordButton.innerHTML = "Record"
	$('#final-modal').modal('show');
}



//helper function
// function __log(e, data) {
// 	log.innerHTML += "\n" + e + " " + (data || '');
// }
